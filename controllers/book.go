package controllers

import (
	"GinSistemPerpustakaan/book"
	"GinSistemPerpustakaan/model"
	"GinSistemPerpustakaan/utils"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// BookController ...
type BookController struct {
	bookService book.BookService
}

// CreateBookController ...
func CreateBookController(router *gin.Engine, bookService book.BookService) {
	inDB := BookController{bookService}

	router.POST("/book", inDB.insertData)
	router.GET("/book", inDB.getAll)
	router.GET("/book/:id", inDB.getBookByID)
	router.PUT("/book/:id", inDB.updateData)
	router.DELETE("/book/:id", inDB.delete)

}

func (h *BookController) insertData(c *gin.Context) {
	book, err := h.getFormData(c)
	if err != nil {
		utils.HandleError(c, http.StatusNotFound, err.Error())
		return
	}

	data, err := h.bookService.Insert(book)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		fmt.Printf("[BookController.Insert]Error when request data to usecase with error : %w\n", err)
		return
	}

	utils.HandleSuccess(c, http.StatusOK, data)
}

func (h *BookController) getFormData(c *gin.Context) (*model.Book, error) {
	title := c.Request.FormValue("title")
	author := c.Request.FormValue("author")
	publisher := c.Request.FormValue("publisher")
	bookyear, err := strconv.Atoi(c.Request.FormValue("book_year"))
	if err != nil {
		return nil, fmt.Errorf("Book year must be a number")
	}
	synopsis := c.Request.FormValue("synopsis")

	qtybook, err := strconv.Atoi(c.Request.FormValue("qty_book"))
	if err != nil {
		return nil, fmt.Errorf("Quantity book must be a number")
	}
	image, err := utils.UploadImage(c, title, "image", "book")
	if err != nil {
		return nil, err
	}

	book := model.Book{
		Title:        title,
		Author:       author,
		Publisher:    publisher,
		BookYear:     bookyear,
		Synopsis:     synopsis,
		QtyBook:      qtybook,
		QtyAvailable: qtybook,
		Image:        image,
	}

	return &book, nil
}

func (h *BookController) getAll(c *gin.Context) {
	data, err := h.bookService.GetAll()
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Ooops something error")

		fmt.Printf("[BookController.getAll] Error when request data to usecase with error: %v\n", err)
		return
	}
	utils.HandleSuccess(c, http.StatusOK, data)
}

func (h *BookController) getBookByID(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "ID NOT VALID !!!")
		fmt.Printf("[BookController.getBookByID] Error when convert pathvar with error: %v\n ", err)
	}

	data, err := h.bookService.GetBookByID(id)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Ooops something error")
		fmt.Printf("[BookController.getBookByID] Error when request data to usecase with error %v\n", err)
		return
	}
	utils.HandleSuccess(c, http.StatusOK, data)
}

func (h *BookController) getFormDataForUpdate(c *gin.Context) (*model.Book, error) {
	title := c.Request.FormValue("title")
	author := c.Request.FormValue("author")
	publisher := c.Request.FormValue("publisher")
	bookyear, err := strconv.Atoi(c.Request.FormValue("book_year"))
	if err != nil {
		return nil, fmt.Errorf("Book year must be a number")
	}
	synopsis := c.Request.FormValue("synopsis")

	book := model.Book{
		Title:     title,
		Author:    author,
		Publisher: publisher,
		BookYear:  bookyear,
		Synopsis:  synopsis,
	}

	return &book, nil
}

func (h *BookController) updateData(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "ID NOT VALID !!!")
		fmt.Printf("[BookController.getBookByID] Error when convert pathvar with error: %v\n ", err)
	}

	book, err := h.getFormDataForUpdate(c)
	if err != nil {
		utils.HandleError(c, http.StatusNotFound, err.Error())
		return
	}

	image, _ := utils.UploadImage(c, book.Title, "image", "book")

	if image == "" {
		book1, _ := h.bookService.GetBookByID(id)
		book.Image = book1.Image
	} else {
		book.Image = image
	}

	data, err := h.bookService.Update(id, book)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		fmt.Printf("[BookController.Insert]Error when request data to usecase with error : %w\n", err)
		return
	}

	utils.HandleSuccess(c, http.StatusOK, data)
}

func (h *BookController) delete(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "ID not valid")
		fmt.Printf("[BookController.delete]Error when convert pathvar with error : %v\n", err)
	}

	err = h.bookService.Delete(id)
	if err != nil {
		utils.HandleError(c, http.StatusNoContent, "Oppss, something error")
		fmt.Printf("[BookController.delete]Error when request data to usecase with error : %v\n", err)
		return
	}
	utils.HandleSuccess(c, http.StatusOK, nil)
}
