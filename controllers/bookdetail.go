package controllers

import (
	"GinSistemPerpustakaan/bookdetail"
	"GinSistemPerpustakaan/utils"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// BookDetailController ...
type BookDetailController struct {
	bookDetailService bookdetail.BookDetailService
}

// CreateBookDetailController ...
func CreateBookDetailController(router *gin.Engine, bookDetailService bookdetail.BookDetailService) {
	inDB := BookDetailController{bookDetailService}

	router.GET("/detail/book/:id", inDB.getAllBookDetail)

}

func (h *BookDetailController) getAllBookDetail(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "ID NOT VALID !!!")
		fmt.Printf("[BookDetailController.getAllByID] Error when convert pathvar with error: %v\n ", err)
	}

	data, err := h.bookDetailService.GetAllBookDetail(id)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Ooops something error")

		fmt.Printf("[BookDetailController.getAllBookDetail] Error when request data to usecase with error: %v\n", err)
		return
	}
	utils.HandleSuccess(c, http.StatusOK, data)
}
