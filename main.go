package main

import (
	repoBook "GinSistemPerpustakaan/book/repo"
	serviceBook "GinSistemPerpustakaan/book/service"
	repoBookDetail "GinSistemPerpustakaan/bookdetail/repo"
	serviceBookDetail "GinSistemPerpustakaan/bookdetail/service"
	"GinSistemPerpustakaan/config"
	"GinSistemPerpustakaan/controllers"
	"GinSistemPerpustakaan/middleware"
	repoUser "GinSistemPerpustakaan/user/repo"
	serviceUser "GinSistemPerpustakaan/user/service"
	"fmt"
	"log"

	"io"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
)

func init() {
	gotenv.Load()
}

func setupLogOutput() {
	f, _ := os.Create("gin.Log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	db := config.Connect()
	defer db.Close()

	setupLogOutput()
	router := gin.New()
	router.Use(gin.Recovery(), middleware.Logger())

	userRepo := repoUser.CreateUserRepoImpl(db)
	bookDetailRepo := repoBookDetail.CreateBookDetailRepoImpl(db)
	bookRepo := repoBook.CreateBookRepoImpl(db)

	userService := serviceUser.CreateUserServiceImpl(userRepo)
	controllers.CreateUserController(router, userService)

	bookService := serviceBook.CreateBookServiceImpl(bookRepo, bookDetailRepo)
	controllers.CreateBookController(router, bookService)

	bookDetailService := serviceBookDetail.CreateBookDetailServiceImpl(bookDetailRepo, bookRepo)
	controllers.CreateBookDetailController(router, bookDetailService)

	fmt.Println("Starting web server at port : 9093")
	err := router.Run(":9093")
	if err != nil {
		log.Fatal()
	}
}
