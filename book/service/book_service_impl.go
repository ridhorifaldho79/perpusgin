package service

import (
	"GinSistemPerpustakaan/book"
	"GinSistemPerpustakaan/bookdetail"
	"GinSistemPerpustakaan/model"
	"errors"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation"
)

// BookServiceImpl ...
type BookServiceImpl struct {
	bookRepo       book.BookRepo
	bookDetailRepo bookdetail.BookDetailRepo
}

// CreateBookServiceImpl ...
func CreateBookServiceImpl(bookRepo book.BookRepo, bookDetailRepo bookdetail.BookDetailRepo) book.BookService {
	return &BookServiceImpl{bookRepo, bookDetailRepo}
}

// Insert ...
func (b *BookServiceImpl) Insert(data *model.Book) (*model.Book, error) {
	if err := data.Validate(); err != nil {
		return nil, err
	}

	tx := b.bookRepo.BeginTrans()

	data, err := b.bookRepo.Insert(data, tx)
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	for i := 0; i < data.QtyBook; i++ {
		dataBookDetail := model.BookDetail{
			StatusBook: "available",
			BookID:     data.ID,
		}

		_, err = b.bookDetailRepo.Insert(&dataBookDetail, tx)
		if err != nil {
			tx.Rollback()
			return nil, fmt.Errorf("[BookRepoImpl.Insert] Error occured while inserting Book_Detail data to database : %w", err)
		}
	}

	tx.Commit()

	return data, nil
}

// GetAll ...
func (b *BookServiceImpl) GetAll() (*[]model.Book, error) {
	data, err := b.bookRepo.GetAll()
	if err != nil {
		return nil, err
	}

	return data, nil
}

// GetBookByID ...
func (b *BookServiceImpl) GetBookByID(id int) (*model.Book, error) {
	data, err := b.bookRepo.GetBookByID(id)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// Delete ...
func (b *BookServiceImpl) Delete(id int) error {
	_, err := b.bookRepo.GetBookByID(id)
	if err != nil {
		return errors.New("bookID does not exist")
	}

	tx := b.bookRepo.BeginTrans()

	err = b.bookRepo.Delete(id, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = b.bookDetailRepo.Delete(id, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	return nil
}

// Update ...
func (b *BookServiceImpl) Update(id int, data *model.Book) (*model.Book, error) {
	firstData, err := b.bookRepo.GetBookByID(id)
	if err != nil {
		return nil, errors.New("bookID does not exist")
	}

	data.QtyBook = firstData.QtyBook
	data.QtyAvailable = firstData.QtyAvailable

	if data.Image == "" {
		data.Image = firstData.Image
	}
	if data.Title == "" {
		data.Title = firstData.Title
	}
	if data.Author == "" {
		data.Author = firstData.Author
	}
	if data.Publisher == "" {
		data.Publisher = firstData.Publisher
	}
	if data.Synopsis == "" {
		data.Synopsis = firstData.Synopsis
	}
	
	if err := validation.Validate(data.BookYear, validation.Required); err != nil {
		return nil, errors.New("book year cannot be blank")
	}

	dataFix, err := b.bookRepo.Update(id, data)
	if err != nil {
		return nil, err
	}

	return dataFix, nil
}
