package book

import (
	"GinSistemPerpustakaan/model"

	"github.com/jinzhu/gorm"
)

type BookRepo interface {
	BeginTrans() *gorm.DB
	Insert(data *model.Book, tx *gorm.DB) (*model.Book, error)
	GetAll() (*[]model.Book, error)
	GetBookByID(id int) (*model.Book, error)
	Delete(id int, tx *gorm.DB) error
	Update(id int, data *model.Book) (*model.Book, error)
}
