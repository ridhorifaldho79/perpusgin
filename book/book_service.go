package book

import "GinSistemPerpustakaan/model"

type BookService interface {
	Insert(data *model.Book) (*model.Book, error)
	GetAll() (*[]model.Book, error)
	GetBookByID(id int) (*model.Book, error)
	Delete(id int) error
	Update(id int, data *model.Book) (*model.Book, error)
}
