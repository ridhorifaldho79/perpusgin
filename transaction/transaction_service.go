package transaction

import "GinSistemPerpustakaan/model"

// TransactionService ...
type TransactionService interface {
	Insert(data *model.Transaksi) (*model.Transaksi, error)
	GetAll() (*[]model.Transaksi, error)
	GetByID(id int) (*model.Transaksi, error)
}
