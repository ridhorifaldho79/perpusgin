package repo

import (
	"GinSistemPerpustakaan/model"
	"GinSistemPerpustakaan/transaction"
	"fmt"

	"github.com/jinzhu/gorm"
)

// TransactionRepoImpl ...
type TransactionRepoImpl struct {
	db *gorm.DB
}

// CreateTransactionRepoImspl ...
func CreateTransactionRepoImspl(db *gorm.DB) transaction.TransactionRepo {
	return &TransactionRepoImpl{db}
}

// Insert ...
func (b *TransactionRepoImpl) Insert(data *model.Transaksi, tx *gorm.DB) (*model.Transaksi, error) {
	
	err := tx.Save(&data).Error
	if err != nil {
		return nil, fmt.Errorf("[TransaksiRepoImpl.Insert] Error occured while inserting Book data to database : %w", err)
	}

	return data, nil
}

// GetAll ...
func (b *TransactionRepoImpl) GetAll() (*[]model.Transaksi, error) {
	var data []model.Transaksi
	err := b.db.Find(&data).Error
	if err != nil {
		return nil, fmt.Errorf("[TransaksiRepoImpl.GetAll] Error when query get by id with error: %w", err)
	}

	return &data, nil
}

// GetByID ...
func (b *TransactionRepoImpl) GetByID(id int) (*model.Transaksi, error) {
	var data model.Transaksi
	err := b.db.First(&data, id).Error
	if err != nil {
		return nil, fmt.Errorf("[TransaksiRepoImpl.GetByID] Error when query get by id with error: %w", err)
	}
	return &data, nil
}
