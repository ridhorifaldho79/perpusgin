package service

import (
	"GinSistemPerpustakaan/book"
	"GinSistemPerpustakaan/model"
	"GinSistemPerpustakaan/transaction"
	"errors"
)

// TransactionServiceImpl ...
type TransactionServiceImpl struct {
	transactionRepo transaction.TransactionRepo
	bookRepo        book.BookRepo
}

// CreateTransactionUsecaseImpl ...
func CreateTransactionUsecaseImpl(transactionRepo transaction.TransactionRepo, bookRepo book.BookRepo) transaction.TransactionService {
	return &TransactionServiceImpl{transactionRepo, bookRepo}
}

func (b *TransactionServiceImpl) Insert(data *model.Transaksi) (*model.Transaksi, error) {
	if err := data.Validate(); err != nil {
		return nil, err
	}

	_, err := b.bookRepo.GetBookByID(int(data.BookID))
	if err != nil {
		return nil, errors.New("book ID does not exist")
	}

	_, err = b.userRepo.GetByID(int(data.UserID))
	if err != nil {
		return nil, errors.New("User ID does not exist")
	}

	dataok, err := b.transaksiRepo.Insert(data)
	if err != nil {
		return nil, err
	}

	return dataok, nil
}

func (b *TransaksiUsecaseImpl) GetAll() (*[]model.Transaksi, error) {

	data, err := b.transaksiRepo.GetAll()
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (b *TransaksiUsecaseImpl) GetByID(id int) (*model.Transaksi, error) {
	data, err := b.transaksiRepo.GetByID(id)
	if err != nil {
		return nil, err
	}

	return data, nil
}
