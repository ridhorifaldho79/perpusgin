package transaction

import (
	"GinSistemPerpustakaan/model"

	"github.com/jinzhu/gorm"
)

// TransactionRepo ...
type TransactionRepo interface {
	Insert(data *model.Transaksi, tx *gorm.DB) (*model.Transaksi, error)
	GetAll() (*[]model.Transaksi, error)
	GetByID(id int) (*model.Transaksi, error)
}
