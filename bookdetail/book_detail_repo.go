package bookdetail

import (
	"GinSistemPerpustakaan/model"

	"github.com/jinzhu/gorm"
)

type BookDetailRepo interface {
	Insert(data *model.BookDetail, tx *gorm.DB) (*model.BookDetail, error)
	GetAllBookDetail(id int) (*[]model.BookDetail, error)
	Delete(id int, tx *gorm.DB) error
}
