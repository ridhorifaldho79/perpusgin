package service

import (
	"GinSistemPerpustakaan/book"
	"GinSistemPerpustakaan/bookdetail"
	"GinSistemPerpustakaan/model"
	"errors"
)

// BookDetailServiceImpl ...
type BookDetailServiceImpl struct {
	bookDetailRepo bookdetail.BookDetailRepo
	bookRepo       book.BookRepo
}

// CreateBookDetailServiceImpl ...
func CreateBookDetailServiceImpl(bookDetailRepo bookdetail.BookDetailRepo, bookRepo book.BookRepo) bookdetail.BookDetailService {
	return &BookDetailServiceImpl{bookDetailRepo, bookRepo}
}

// GetAllBookDetail ...
func (b *BookDetailServiceImpl) GetAllBookDetail(id int) (*[]model.BookDetail, error) {
	_, err := b.bookRepo.GetBookByID(id)
	if err != nil {
		return nil, errors.New("category ID does not exist")
	}

	data, err := b.bookDetailRepo.GetAllBookDetail(id)
	if err != nil {
		return nil, err
	}

	return data, nil
}
