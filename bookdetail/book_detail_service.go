package bookdetail

import "GinSistemPerpustakaan/model"

type BookDetailService interface {
	GetAllBookDetail(id int) (*[]model.BookDetail, error)
}
