package repo

import (
	"GinSistemPerpustakaan/bookdetail"
	"GinSistemPerpustakaan/model"
	"fmt"

	"github.com/jinzhu/gorm"
)

// BookDetailRepoImpl ...
type BookDetailRepoImpl struct {
	db *gorm.DB
}

// CreateBookDetailRepoImpl ...
func CreateBookDetailRepoImpl(db *gorm.DB) bookdetail.BookDetailRepo {
	return &BookDetailRepoImpl{db}
}

// Insert ...
func (b *BookDetailRepoImpl) Insert(data *model.BookDetail, tx *gorm.DB) (*model.BookDetail, error) {
	err := tx.Save(&data).Error
	if err != nil {
		return nil, fmt.Errorf("[BookDetailRepoImpl.Insert] Error occured while inserting BookDetail data to database : %w", err)
	}

	return data, nil
}

// GetAllBookDetail ...
func (b *BookDetailRepoImpl) GetAllBookDetail(id int) (*[]model.BookDetail, error) {
	var data []model.BookDetail
	err := b.db.Where("book_id = ?", id).Find(&data).Error
	if err != nil {
		return nil, fmt.Errorf("[BookRepoImpl.GetAllBookDetail] Error when query get all data with error: %w", err)
	}
	return &data, nil
}

// Delete ...
func (b *BookDetailRepoImpl) Delete(id int, tx *gorm.DB) error {
	dataDetailBook := model.BookDetail{}

	err := b.db.Where("book_id=?", id).Delete(&dataDetailBook).Error
	if err != nil {
		return fmt.Errorf("[BookRepoImpl.Delete] Error when query delete data with error: %w", err)
	}

	return nil
}
