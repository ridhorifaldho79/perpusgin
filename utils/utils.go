package utils

import (
	"GinSistemPerpustakaan/model"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/gomail.v2"
)

const CONFIG_SMTP_HOST = "smtp.gmail.com"
const CONFIG_SMTP_PORT = 587

// HandleError ...
func HandleError(c *gin.Context, status int, msg string) *gin.Context {
	c.JSON(status, gin.H{
		"Success": "False",
		"Message": msg,
	})
	return c
}

// HandleSuccess ...
func HandleSuccess(c *gin.Context, status int, data interface{}) *gin.Context {
	c.JSON(status, gin.H{
		"Success": "True",
		"Message": "Success",
		"data":    data,
	})
	return c
}

// HashedPassword ...
func HashedPassword(c *gin.Context, user model.User) *model.User {

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 10)
	if err != nil {
		HandleError(c, http.StatusBadRequest, "ooppss, something when wrong")
		fmt.Println("[HashedPassword]Error occured while hashing password ", err.Error())
		return nil
	}

	user.Password = string(hashedPassword)

	return &user
}

// ValidateLogin ...
func ValidateLogin(c *gin.Context, user *model.User) bool {
	if user.Email == "" {
		HandleError(c, http.StatusBadRequest, "Email cannot be empty")
		fmt.Println("[ValidateLogin] Error when validate email")
		return false
	}
	if user.Password == "" {
		HandleError(c, http.StatusBadRequest, "Password cannot be empty")
		fmt.Println("[ValidateLogin]")
		return false
	}
	return true
}

// CheckValidMail ...
func CheckValidMail(email string) bool {
	regex := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return regex.MatchString(email)
}

// ComparePassword ...
func ComparePassword(HashedPassword string, password []byte) bool {
	err := bcrypt.CompareHashAndPassword([]byte(HashedPassword), password)
	if err != nil {
		return false
	}
	return true
}

// GenerateToken ...
func GenerateToken(user *model.User) (string, error) {

	secret := os.Getenv("SECRET")
	fmt.Println(user.Username)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"ID":       user.ID,
		"username": user.Username,
		"email":    user.Email,
		"role":     user.Role,
	})

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Fatalln(err)
	}

	return tokenString, nil
}

// SendEmail ...
func SendEmail(data *model.User) {

	var toEmail = data.Email
	email := os.Getenv("CONFIG_EMAIL")
	password := os.Getenv("CONFIG_PASSWORD")
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", email)
	mailer.SetHeader("To", toEmail)
	mailer.SetAddressHeader("Cc", "tralalala@gmail.com", "Tra Lala La")
	mailer.SetHeader("Subject", "Verifikasi Email")
	mailer.SetBody("text/html", fmt.Sprintf("Hello %s! <br> Welcome to my paradise", data.Username))

	dialer := gomail.NewDialer(
		CONFIG_SMTP_HOST,
		CONFIG_SMTP_PORT,
		email,
		password,
	)

	err := dialer.DialAndSend(mailer)
	if err != nil {
		fmt.Println("kesini mih kiirmi emaildsada")
		log.Fatal(err)
	}

	log.Println("Mail sent!!")
}

//UploadImage represent a method to upload image to local
func UploadImage(c *gin.Context, name string, key string, dest string) (string, error) {
	fileServer := os.Getenv("IMAGE_SERVER_URL")
	file, fileHandler, err := c.Request.FormFile(key)
	if err != nil {
		return "", fmt.Errorf("Unable to open" + strings.ToUpper(key) + " file")
	}
	defer file.Close()
	splitedName := strings.Split(fileHandler.Filename, ".")
	extension := "." + splitedName[len(splitedName)-1]
	newName := strings.ReplaceAll(name, " ", "")
	fileName := "/" + newName + "-" + strings.ToUpper(key) + extension

	tempFile := filepath.Join(dest + fileName)

	isValid, msg := ValidateImageExtension(extension, int(fileHandler.Size))
	if !isValid {
		return fileServer + tempFile, fmt.Errorf(strings.ToUpper(key) + msg)
	}

	targetFile, err := os.OpenFile("assets/"+tempFile, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return fileServer + tempFile, fmt.Errorf("Unable to copy " + strings.ToUpper(key) + " file")
	}

	defer targetFile.Close()

	if _, err := io.Copy(targetFile, file); err != nil {
		return tempFile, fmt.Errorf("Unable to save " + strings.ToUpper(key) + " file")
	}
	return fileServer + tempFile, nil
}

//UploadImageForUpdate represent a method to upload image to local
func UploadImageForUpdate(c *gin.Context, name string, key string, dest string) (string, error) {
	fileServer := os.Getenv("IMAGE_SERVER_URL")
	file, fileHandler, err := c.Request.FormFile(key)
	if err != nil {
		return "", fmt.Errorf("Unable to open" + strings.ToUpper(key) + " file")
	}
	defer file.Close()
	splitedName := strings.Split(fileHandler.Filename, ".")
	extension := "." + splitedName[len(splitedName)-1]
	newName := strings.ReplaceAll(name, " ", "")
	fileName := "/" + newName + "-" + strings.ToUpper(key) + extension

	tempFile := filepath.Join(dest + fileName)

	isValid, msg := ValidateImageExtension(extension, int(fileHandler.Size))
	if !isValid {
		return fileServer + tempFile, fmt.Errorf(strings.ToUpper(key) + msg)
	}

	targetFile, err := os.OpenFile("assets/"+tempFile, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return fileServer + tempFile, fmt.Errorf("Unable to copy " + strings.ToUpper(key) + " file")
	}

	defer targetFile.Close()

	if _, err := io.Copy(targetFile, file); err != nil {
		return tempFile, fmt.Errorf("Unable to save " + strings.ToUpper(key) + " file")
	}
	return fileServer + tempFile, nil
}

// ValidateImageExtension ...
func ValidateImageExtension(image string, size int) (bool, string) {
	if image != ".jpg" && image != ".png" && image != ".jpeg" {
		return false, " file must be .jpg/.jpeg/.png"
	}
	if size >= 1000024 {
		return false, " file max size is 1MB"
	}
	return true, ""
}
